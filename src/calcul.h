/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

#ifndef _CALCUL_H_
#define _CALCUL_H_

#include <vector>

bool Bissextile_Julienne(int nAnnee);
bool Bissextile_Gregorienne(int nAnnee);
bool DateValide_Julienne(int nAnnee, int nMois, int nJour, double dFracDJour);
bool DateValide_Gregorienne(int nAnnee, int nMois, int nJour, double dFracDJour);
int ConvertirGregorienneEnJourJulien(int nAnnee, int nMois, int nJour, double dFracDJour, double& dJj);
int ConvertirJulienneEnJourJulien(int nAnnee, int nMois, int nJour, double dFracDJour, double& dJj);
int ConvertirJourJulienEnGregorienne(double dJj, int& nAnnee, int& nMois, int& nJour, double& dFracDJour);
int ConvertirJourJulienEnJulienne(double dJj, int& nAnnee, int& nMois, int& nJour, double& dFracDJour);
int JourDeLaSemaine(double dJj);
int JourDeLaSemaine_Gregorienne(int nAnnee, int nMois, int nJour, int& nJourDSemaine);
int JourDeLaSemaine_Julienne(int nAnnee, int nMois, int nJour, int& nJourDSemaine);
int ConvertirJulienneEnGregorienne(int nAnnee_Juli, int nMois_Juli, int nJour_Juli, int& nAnnee_Greg, int& nMois_Greg, int& nJour_Greg);
bool DateDePaquesValide(int nMois, int nJour);
int Paques_Greg(int nAnnee, int& nMois, int& nJour);
int Paques_Juli(int nAnnee, int& nMois, int& nJour);
int TrouverFetes(void);
int TrouverAnnees(void);
int ListerDates(void);
int TrouvLundi(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvCendres(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvRameaux(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvVendredi(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvPaques(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvAscension(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvPentecote(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvAvent(int& nMois, int& nJour, int& nJourDeSemaine);
int TrouvNoel(int& nMois, int& nJour, int& nJourDeSemaine);
int Annees_Lundi(std::vector<int>& vanAnnees);
int Annees_Cendres(std::vector<int>& vanAnnees);
int Annees_Rameaux(std::vector<int>& vanAnnees);
int Annees_Vendredi(std::vector<int>& vanAnnees);
int Annees_Paques(std::vector<int>& vanAnnees);
int Annees_Ascension(std::vector<int>& vanAnnees);
int Annees_Pentecote(std::vector<int>& vanAnnees);
int Annees_Avent(std::vector<int>& vanAnnees);
int Annees_Noel(std::vector<int>& vanAnnees);

#endif // _CALCUL_H_

