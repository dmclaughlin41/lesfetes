/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

#ifndef _AFFICHAGE_H_
#define _AFFICHAGE_H_

#include <vector>

int OptionsInvalides(void);
int MessageDErreur(void);
int AfficherLaVersion(void);
int Assist(void);
int AfficherDate(int nAnnee, int nMois, int nJour, int nJourDeSemaine, char* szFete);
int AfficherAnnees(std::vector<int>& vanAnnees, char *szFete);

#endif // _AFFICHAGE_H_

