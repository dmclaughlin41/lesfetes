/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

#ifndef _UNIVERSALS_H_
#define _UNIVERSALS_H_

// Les variables globales
extern int g_nAnnee;
extern int g_nPremiereAnnee;
extern int g_nDerniereAnnee;
extern int g_nMoisFete;
extern int g_nJourFete;
extern int g_nAnnee_JuliChangeGreg;
extern bool g_blOrthodoxe;
extern bool g_blAlexandrine;
extern bool g_blGregorienne;
extern bool g_blJuliAGreg;
extern bool g_blAucuneCouleur;
extern bool g_blLundiPur;
extern bool g_blCendres;
extern bool g_blRameaux;
extern bool g_blVendredi;
extern bool g_blPaques;
extern bool g_blAscension;
extern bool g_blPentecote;
extern bool g_blAvent;
extern bool g_blNoel;

// Les tableaux des noms
extern const char *g_szMoises[];
extern const char *g_szJoursDeSemaine[];
extern const char *g_szLesFetes[];

// Références nomées aux noms des fêtes
#define LUNDI_PUR          0
#define MERCREDI_CENDRES   1
#define DIMANCHE_RAMEAUX   2
#define VENDREDI_SAINT     3
#define PAQUES             4
#define JEUDI_ASCENSION    5
#define DIMANCHE_PENTECOTE 6
#define AVENT              7
#define NOEL               8

// Les signaux des erreurs
//
// Cette valeur indique que la fonction a traité normalement
#define NORMAL \
0b0000000000000000
//
// Cette valeur indique qu'un argument donné à la fonction n'appartient
// pas à la plage des valeurs autorisées
#define VALEUR_AUTORISEE \
0b0000000000000001
//
// Cette valeur indique qu'un argument donné à la fonction n'est pas
// approprié pour la fonction mais cela ne causera probablement pas une
// erreur
#define VALEUR_APPROPRIEE \
0b0000000000000010
//
// Cette valeur indique qu'un calcul dans la fonction a échoué
#define ERREUR_CALCUL \
0b0000000000000100
//
// Cette valeur indique que la fonction a été échoué
#define ERREUR_ECHEC \
0b0000000000001000
//
// Cette valeur indique qu'un argument de la fonction est invalid
#define ARGUMENT_INVALIDE \
0b0000000000010000

#endif // _UNIVERSALS_H_

