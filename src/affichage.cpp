/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

#include "affichage.h"
#include "calcul.h"
#include "config.h"
#include "universals.h"
#include <cstdio>
#include <vector>

int OptionsInvalides() {
  printf("Vous avez saisi des options invalides.\n\n");
  Assist();
  return 0;
}

int MessageDErreur() {
  printf("Il y a une erreur...\n");
  return 0;
}

int AfficherLaVersion() {
  printf(
PACKAGE_STRING "\n"
"Copyright © 2018 Daniel McLaughlin\n"
"License GPLv3+ : GNU GPL version 3 ou ultérieure\n"
"<http://gnu.org/licenses/gpl.html>\n"
"C'est logiciel libre, vous êtes libre de le modifier et de le\n"
"redistribuer.\n"
"Ce logiciel n'est accompagné d'ABSOLUMENT AUCUNE GARANTIE, dans les\n"
"limites autorisées par la loi applicable.\n"
"\n"
"Écrit par Daniel McLaughlin.\n"
  );
  return 0;
}

int Assist() {
  printf(
"Usage: " PACKAGE " [options]\n"
"\n"
"Il y a trois modes de fonctionnement :\n"
" * On trouve les dates des fêtes spécifiées en une seule année\n"
"       Ce mode nécessite l'option --annee\n"
" * On trouve les années où une fête spécifiée tombe à une date\n"
"   précise\n"
"       Ce mode necessite les options --premiere-annee\n"
"       --derniere-annee et --date\n"
" * On énumère les dates pour une fête spécifiée dans une gamme\n"
"   d'années\n"
"       Ce mode nécessite les options --premiere-annee et\n"
"       --derniere-annee\n"
"\n"
"Les options de fonctionnement :\n"
" --annee=YYYY\n"
"    On spécifie l'année pour une seule année\n"
" --premiere-annee=YYYY\n"
"    On spécifie la première année dans une gamme d'années\n"
" --derniere-annee=YYYY\n"
"    On spécifie la dernière année dans une gamme d'années\n"
" --date=DD/MM\n"
"    On spécifie la date de la fête\n"
"\n"
"Les options des fêtes :\n"
"Ces options spécifient la/les fête(s) pour l'opération\n"
" --lundi-pur\n"
" --mercredi-des-cendres\n"
" --dimanche-des-rameaux\n"
" --vendredi-saint\n"
" --paques\n"
" --jeudi-d-ascension\n"
" --dimanche-de-pentecote\n"
" --avent\n"
" --noel\n"
" --toutes-les-fetes\n"
"    cette option spécifie toutes les fêtes\n"
"Une seule option de fête peut être utilisé pour les modes de trouver\n"
"les années et d'énumération. Toute combinaison peut être utilisée\n"
"pour la mode de trouver les dates.\n"
"Si aucune option de fête n'est spécifiée, Pâques est implicite.\n"
"\n"
"Les options du calcul :\n"
" --alexandrine\n"
"    On utilise la méthode alexandrine pour toutes les années\n"
" --gregorienne\n"
"    On utilise la méthode grégorienne pour toutes les années, avant\n"
"    même avant la réforme de Grégoire XIII\n"
" --annee-changeant=YYYY\n"
"    On spécifie la première année pour l'utilisation de la méthode\n"
"    grégorienne. Si cette option n'est pas spécifiée, l'année 1583\n"
"    est implicite.\n"
"Si aucune option n'est choisie ou les deux options sont choisies,\n"
"l'utilisation de la méthode alexandrine avant la réforme grégorienne\n"
"et l'utilisation de la méthode grégorienne après la réforme est\n"
"implicite.\n"
"\n"
"Les autres options :\n"
" --conv-juli-a-greg\n"
"    Cette option affiche une date en les deux julienne et\n"
"    grégorienne\n"
" --orthodoxe\n"
"    Si l'élise orthodoxe utilise une méthode differente pour\n"
"    calculer la date, cette option utilise la méthode orthodoxe\n"
"    Cela n'a aucun effet lors du calcul avec l'option --alexandrine\n"
"    pour le calcul de Pâques car l'élise orthodoxe utilise déjà\n"
"    cette méthode toujours\n"
" --aucune-couleur\n"
"    Cette option désactive l'utilisation de la couleur sur le\n"
"    terminal\n"
" --version\n"
"    On affiche la version de ce logicel\n"
" --aide\n"
"    On affiche ce message\n"
"\n"
"Pour en savoir plus sur ce logicel, saisissez « man " PACKAGE " ».\n"
  );
  return 0;
}



int AfficherDate(int nAnnee, int nMois, int nJour, int nJourDeSemaine,
                                                         char* szFete) {
  if (g_blAucuneCouleur) {
    if (g_blJuliAGreg) {
      int nX, nMois_G, nJour_G;
      ConvertirJulienneEnGregorienne(nAnnee, nMois, nJour, nX, nMois_G,
                                                               nJour_G);
      printf("%s %d : %s %d %s A.S. --> %d %s N.S.\n", szFete, nAnnee,
             g_szJoursDeSemaine[nJourDeSemaine], nJour,
             g_szMoises[nMois - 1], nJour_G, g_szMoises[nMois_G - 1]);
    } else {
      printf("%s %d : %s %d %s\n", szFete, nAnnee,
             g_szJoursDeSemaine[nJourDeSemaine], nJour,
             g_szMoises[nMois - 1]);
    }
  } else {
    if (g_blJuliAGreg) {
      int nX, nMois_G, nJour_G;
      ConvertirJulienneEnGregorienne(nAnnee, nMois, nJour, nX, nMois_G,
                                                               nJour_G);
      printf("\033[1;97m%s %d : \033[0m\033[3;93m%s %d %s A.S. "
             "\033[3;31m--> %d %s N.S.\033[0m\n", szFete, nAnnee,
             g_szJoursDeSemaine[nJourDeSemaine], nJour,
             g_szMoises[nMois - 1], nJour_G, g_szMoises[nMois_G - 1]);
    } else {
      printf("\033[1;97m%s %d : \033[0m\033[3;93m%s %d %s\033[0m\n",
             szFete, nAnnee, g_szJoursDeSemaine[nJourDeSemaine], nJour,
             g_szMoises[nMois - 1]);
    }
  }

  return 0;
}

int AfficherAnnees(std::vector<int>& vanAnnees, char *szFete) {
  int nCol = 0;
  if (g_blAucuneCouleur) {
    printf("Les années entre %d et %d à %s le %d %s :\n",
           g_nPremiereAnnee, g_nDerniereAnnee, szFete, g_nJourFete,
           g_szMoises[g_nMoisFete - 1]);
    for (unsigned n = 0; n < vanAnnees.size(); n++) {
      printf("%d  ", vanAnnees[n]);
      nCol++;
      if (nCol > 7) {
        nCol = 0;
        printf("\n");
      }
    }
    if (vanAnnees.size() == 0) {
      printf("\nIl n'y a pas d'années\n");
    } else if (vanAnnees.size() == 1) {
      printf("\nIl y a une année\n");
    } else {
      printf("\nIl y a %lu années\n", vanAnnees.size());
    }
  } else {
    printf("\033[1;97mLes années entre %d et %d à %s le %d %s :"
           "\033[0m\033[3;93m\n", g_nPremiereAnnee, g_nDerniereAnnee,
           szFete, g_nJourFete, g_szMoises[g_nMoisFete - 1]);
    for (unsigned n = 0; n < vanAnnees.size(); n++) {
      printf("%d  ", vanAnnees[n]);
      nCol++;
      if (nCol > 7) {
        nCol = 0;
        printf("\n");
      }
    }
    printf("\033[3;31m");
    if (vanAnnees.size() == 0) {
      printf("\nIl n'y a pas d'années\033[0m\n");
    } else if (vanAnnees.size() == 1) {
      printf("\nIl y a une année\033[0m\n");
    } else {
      printf("\nIl y a %lu années\033[0m\n", vanAnnees.size());
    }
  }
  return 0;
}

