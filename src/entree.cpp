/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

#include "affichage.h"
#include "calcul.h"
#include "entree.h"
#include "universals.h"
#include <cstdio>
#include <getopt.h>

// Le « struct » pour les options
static struct option long_options[] = {

  // Les options de la fonction
  { "annee",          required_argument, 0, 'a' },
  { "premiere-annee", required_argument, 0, 'm' },
  { "derniere-annee", required_argument, 0, 'n' },
  { "date",           required_argument, 0, 'd' },

  // Les options des fêtes
  { "lundi-pur",             no_argument, 0, 'u' },
  { "mercredi-des-cendres",  no_argument, 0, 'c' },
  { "dimanche-des-rameaux",  no_argument, 0, 'r' },
  { "vendredi-saint",        no_argument, 0, 'i' },
  { "paques",                no_argument, 0, 'p' },
  { "jeudi-d-ascension",     no_argument, 0, 's' },
  { "dimanche-de-pentecote", no_argument, 0, 't' },
  { "avent",                 no_argument, 0, 'v' },
  { "noel",                  no_argument, 0, 'l' },
  { "toutes-les-fetes",      no_argument, 0, 1006 },

  // Les options de la méthode
  { "alexandrine",      no_argument,       0, 1001 },
  { "gregorienne",      no_argument,       0, 1002 },
  { "conv-juli-a-greg", no_argument,       0, 1005 },
  { "annee-changeant",  required_argument, 0, 1003 },
  { "orthodoxe",        no_argument,       0, 1004 },

  // Les autres options
  { "aide",           no_argument, 0, 1000 },
  { "version",        no_argument, 0, 1008 },
  { "aucune-couleur", no_argument, 0, 1007 },

  // Cette ligne est nécessaire pour le terminer du « struct »
  { 0, 0, 0, 0 }
};


int main(int argc, char** argv) {

  // Mettre les valeurs défautes
  g_nAnnee = -1;
  g_nPremiereAnnee = -1;
  g_nDerniereAnnee = -1;
  g_nMoisFete = -1;
  g_nJourFete = -1;
  g_nAnnee_JuliChangeGreg = -1;
  g_blOrthodoxe = false;
  g_blAlexandrine = false;
  g_blGregorienne = false;
  g_blJuliAGreg = false;
  g_blAucuneCouleur = false;
  g_blLundiPur = false;
  g_blCendres = false;
  g_blRameaux = false;
  g_blVendredi = false;
  g_blPaques = false;
  g_blAscension = false;
  g_blPentecote = false;
  g_blAvent = false;
  g_blNoel = false;
  bool blAnnee = false;
  bool blPremAn = false;
  bool blDernAn = false;
  bool blDate = false;
  bool blJuliChangeGreg = false;
  bool blErreurArg = false;

  // Lire les options
  int nIndice = 0;
  int nOpt = 0;
  int nRes;
  while (
    (nOpt = getopt_long(argc, argv, "a:m:n:d:ucripstvl",
                        long_options, &nIndice)) != -1
  ) {
    switch(nOpt) {
      case 'a': // --annee=YYYY
        nRes = sscanf(optarg, "%d", &g_nAnnee);
        if (nRes != 1)
          blErreurArg = true;
        blAnnee = true;
        break;
      case 'm': // --premiere-annee=YYYY
        nRes = sscanf(optarg, "%d", &g_nPremiereAnnee);
        if (nRes != 1)
          blErreurArg = true;
        blPremAn = true;
        break;
      case 'n': // --dernier-annee=YYYY
        nRes = sscanf(optarg, "%d", &g_nDerniereAnnee);
        if (nRes != 1)
          blErreurArg = true;
        blDernAn = true;
        break;
      case 'd': // --date=DD/MM
        nRes = sscanf(optarg, "%d/%d", &g_nJourFete, &g_nMoisFete);
        if (nRes != 2)
          blErreurArg = true;
        blDate = true;
        break;
      case 'u': // --lundi-pur
        g_blLundiPur = true;
        break;
      case 'c': // --mercredi-des-cendres
        g_blCendres = true;
        break;
      case 'r': // --dimanche-des-rameaux
        g_blRameaux = true;
        break;
      case 'i': // --vendredi-saint
        g_blVendredi = true;
        break;
      case 'p': // --paques
        g_blPaques = true;
        break;
      case 's': // --jeudi-d-ascension
        g_blAscension = true;
        break;
      case 't': // --dimanche-de-pentecote
        g_blPentecote = true;
        break;
      case 'v': // --avent
        g_blAvent = true;
        break;
      case 'l': // --noel
        g_blNoel = true;
        break;
      case 1006: // --toutes-les-fetes
        g_blLundiPur = true;
        g_blCendres = true;
        g_blRameaux = true;
        g_blVendredi = true;
        g_blPaques = true;
        g_blAscension = true;
        g_blPentecote = true;
        g_blAvent = true;
        g_blNoel = true;
        break;
      case 1001: // --alexandrine
        g_blAlexandrine = true;
        break;
      case 1002: // --gregorienne
        g_blGregorienne = true;
        break;
      case 1003: // --annee-changeant=YYYY
        blJuliChangeGreg = true;
        nRes = sscanf(optarg, "%d", &g_nAnnee_JuliChangeGreg);
        if (nRes != 1)
          blErreurArg = true;
        break;
      case 1004: // --orthodoxe
        g_blOrthodoxe = true;
        break;
      case 1005: // --conv-juli-a-greg
        g_blJuliAGreg = true;
        break;
      case 1000: // --aide
        Assist();
        break;
      case 1007: // --aucune-couleur
        g_blAucuneCouleur = true;
        break;
      case 1008: // --version
        AfficherLaVersion();
        return 0;
        break;
      default: // option invalide appelée
        OptionsInvalides();
        return -1;
    }
  }

  // Les options sont-elles valides ?
  bool blValide = true;

  // La date(s) de(s) fête(s) pour une seule année ?
  // Ou les années avec une date specifée ?
  // Ou les dates pour une liste des années ?
  bool blTrouverFete = false, blTrouverAnnees = false,
       blListerAnnees = false;
  if (blAnnee)
    blTrouverFete = true;
  if (blPremAn && blDernAn && blDate)
    blTrouverAnnees = true;
  if (blPremAn && blDernAn && !blDate)
    blListerAnnees = true;
  int nNombreDesModes = 0;
  if (blTrouverFete) nNombreDesModes++;
  if (blTrouverAnnees) nNombreDesModes++;
  if (blListerAnnees) nNombreDesModes++;
  if (nNombreDesModes != 1)
    blValide = false;
  if ((blPremAn || blDernAn) && (!(blPremAn && blDernAn)))
    blValide = false;

  // Combien fêtes est spécifiées ?
  int nNombreDesFetes = 0;
  if (g_blLundiPur) nNombreDesFetes++;
  if (g_blCendres) nNombreDesFetes++;
  if (g_blRameaux) nNombreDesFetes++;
  if (g_blVendredi) nNombreDesFetes++;
  if (g_blPaques) nNombreDesFetes++;
  if (g_blAscension) nNombreDesFetes++;
  if (g_blPentecote) nNombreDesFetes++;
  if (g_blAvent) nNombreDesFetes++;
  if (g_blNoel) nNombreDesFetes++;
  // Si aucune fête n'est spécifiée, sélectionner Pâques seulement
  if (nNombreDesFetes == 0) {
    g_blPaques = true;
    nNombreDesFetes = 1;
  }

  // Si trouver les années avec une date spécifiée, combien fêtes ?
  // Il ne peut y avoir plus d'une !
  if (blTrouverAnnees && (nNombreDesFetes != 1))
    blValide = false;

  // Si lister les dates pour une gamme d'années, combien fêtes ?
  // Il ne peut y avoir plus d'une !
  if (blListerAnnees && (nNombreDesFetes != 1))
    blValide = false;

  // Pour le changement d'alexandrine à grégorienne, quelle année ?
  if (!blJuliChangeGreg)   // Par défaut, la première pâques qui utilise
    g_nAnnee_JuliChangeGreg = 1583; // la méthode grégorienne plutôt que
                                   // de la méthode alexandrine est 1583

  // Si ni alexandrine ni grégorienne sont sélectionnées, utiliser les
  // deux, alexandrine avant la réforme grégorienne et grégorienne après
  if ((!(g_blAlexandrine)) && (!(g_blGregorienne))) {
    g_blAlexandrine = true;
    g_blGregorienne = true;
  }

  // Si les options sont invalides, afficher un message et arrêter
  if (!(blValide)) {
    OptionsInvalides();
    return -1;
  }
  if (blErreurArg) {
    OptionsInvalides();
    return -1;
  }

  // Faire le calcul spécifié
  if (blTrouverFete) {
    nRes = TrouverFetes();
  }
  else if (blTrouverAnnees) {
    nRes = TrouverAnnees();
  }
  else if (blListerAnnees) {
    nRes = ListerDates();
  }
  else { return -1; }

  // Renvoyer le nombre zéro
  return 0;
}

