/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

// Les variables globales
int g_nAnnee;
int g_nPremiereAnnee;
int g_nDerniereAnnee;
int g_nMoisFete;
int g_nJourFete;
int g_nAnnee_JuliChangeGreg;
bool g_blOrthodoxe;
bool g_blAlexandrine;
bool g_blGregorienne;
bool g_blJuliAGreg;
bool g_blAucuneCouleur;
bool g_blLundiPur;
bool g_blCendres;
bool g_blRameaux;
bool g_blVendredi;
bool g_blPaques;
bool g_blAscension;
bool g_blPentecote;
bool g_blAvent;
bool g_blNoel;

// Le tableau des noms des moises
const char *g_szMoises[] = {
  (char*)"janvier", (char*)"février",  (char*)"mars",
  (char*)"avril",   (char*)"mai",      (char*)"juin",
  (char*)"juillet", (char*)"août",     (char*)"septembre",
  (char*)"octobre", (char*)"novembre", (char*)"décembre"
};

// Le tableau des noms des jours de la semaine
const char *g_szJoursDeSemaine[] = {
  (char*)"lundi",
  (char*)"mardi",
  (char*)"mercredi",
  (char*)"jeudi",
  (char*)"vendredi",
  (char*)"samedi",
  (char*)"dimanche"
};

// Le tableau des noms des fêtes
const char *g_szLesFetes[] = {
  (char*)"Lundi pur",
  (char*)"Mercredi des cendres",
  (char*)"Dimanche des rameaux",
  (char*)"Vendredi saint",
  (char*)"Pâques",
  (char*)"Jeudi d'ascension",
  (char*)"Dimanche de pentecôte",
  (char*)"Avent",
  (char*)"Noël"
};

