/*  Copyright © 2018 Daniel McLaughlin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at
  your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file is a component of "LesFêtes".
*/

#include "affichage.h"
#include "calcul.h"
#include "universals.h"
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <vector>

using namespace std;

bool Bissextile_Julienne(int nAnnee) {
  return nAnnee % 4 == 0;
}

bool Bissextile_Gregorienne(int nAnnee) {
  if (nAnnee % 400 == 0) return true;
  if (nAnnee % 100 == 0) return false;
  return nAnnee % 4 == 0;
}

bool DateValide_Julienne(int nAnnee, int nMois, int nJour,
                                                    double dFracDJour) {
  bool blValide = true;

  // Est-ce que la fraction du jour invalide ?
  if (dFracDJour < (double)0 || dFracDJour >= (double)1)
    blValide = false;

  // Est-ce que le jour avant le premier du mois ?
  if (nJour < 1)
    blValide = false;

  // Est-ce que le jour après le dernier du mois ?
  if (nMois == 1 || nMois == 3 || nMois == 5 || nMois == 7 ||
                            nMois == 8 || nMois == 10 || nMois == 12) {
    if (nJour > 31) // janv, mars, mai, juil, août, oct et déc
      blValide = false; // 31 jours
  } else if (nMois == 4 || nMois == 6 || nMois == 9 || nMois == 11) {
    if (nJour > 30) // avr, juin, sep et nov
      blValide = false; // 30 jours
  } else if (nMois == 2) {
    if (Bissextile_Julienne(nAnnee)) { // févr
      if (nJour > 29) // bissextile -- 29 jours
        blValide = false;
    } else {
      if (nJour > 28) // ordinaire -- 28 jours
        blValide = false;
    }
  } else { blValide = false; } // le nombre du mois n'est pas valide

  // Est-ce que la date avant l'epoque du jour julien ?
  // le 1 janvier -4712 (ou le 1 janvier 4713 av. J.-C.) à 12h
  if (nAnnee < -4712)
    blValide = false;
  if (nAnnee == -4712) {
    if (nMois == 1 && nJour == 1 && dFracDJour < (double)0.5)
      blValide = false;
  }

  // Renvoyer le résultat de la validation
  return blValide;
}

bool DateValide_Gregorienne(int nAnnee, int nMois, int nJour,
                                                    double dFracDJour) {
  bool blValide = true;

  // Est-ce que la fraction du jour invalide ?
  if (dFracDJour < (double)0 || dFracDJour >= (double)1)
    blValide = false;

  // Est-ce que le jour avant le premier du mois ?
  if (nJour < 1)
    blValide = false;

  // Est-ce que le jour après le dernier du mois ?
  if (nMois == 1 || nMois == 3 || nMois == 5 || nMois == 7 ||
                             nMois == 8 || nMois == 10 || nMois == 12) {
    if (nJour > 31) // janv, mars, mai, juil, août, oct et déc
      blValide = false; // 31 jours
  } else if (nMois == 4 || nMois == 6 || nMois == 9 || nMois == 11) {
    if (nJour > 30) // avr, juin, sep et nov
      blValide = false; // 30 jours
  } else if (nMois == 2) {
    if (Bissextile_Gregorienne(nAnnee)) { // févr
      if (nJour > 29) // bissextile -- 29 jours
        blValide = false;
    } else {
      if (nJour > 28) // ordinaire -- 28 jours
        blValide = false;
    }
  } else { blValide = false; } // le nombre du mois est invalide

  // Est-ce que la date avant l'epoque du jour julien ?
  // le 24 nov -4713 (ou le 24 nov 4714 av. J.-C. [grégorien]) à 12h
  if (nAnnee < -4713)
    blValide = false;
  if (nAnnee == -4713) {
    if (nMois < 11)
      blValide = false;
    if (nMois == 11) {
      if (nJour < 24)
        blValide = false;
      if (nJour == 24) {
        if (dFracDJour < (double)0.5)
          blValide = false;
      }
    }
  }

  // Renvoyer le résultat de la validation
  return blValide;
}

int ConvertirGregorienneEnJourJulien(int nAnnee, int nMois, int nJour,
                                       double dFracDJour, double& dJj) {
  int nRet = NORMAL;

  // Attraper les erreurs dans les arguments
  if (!(DateValide_Gregorienne(nAnnee, nMois, nJour, dFracDJour)))
    nRet |= VALEUR_AUTORISEE;

  // Cet algoritme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 7 : « Julian Day »
  // Les pages de 60 à 61

  int nAn = nAnnee;
  int nMs = nMois;
  double dJr = (double)nJour + dFracDJour;
  if (nMs == 1 || nMs == 2) {
    nAn -= 1;
    nMs += 12;
  }
  int nA = (int)floor((double)nAn / (double)100);
  int nB = 2 - nA + (int)floor((double)nA / (double)4);
  dJj = floor((double)365.25 * (double)(nAn + 4716)) +
        floor((double)30.6001 * (double)(nMs + 1)) + dJr +
        (double)nB - (double)1524.5;
  if (dJj < (double)0)
    nRet |= ERREUR_CALCUL;

  // Renvoyer la valeur du retour
  return nRet;
}

int ConvertirJulienneEnJourJulien(int nAnnee, int nMois, int nJour,
                                       double dFracDJour, double& dJj) {
  int nRet = NORMAL;

  // Attraper les erreurs dans les arguments
  if (!(DateValide_Julienne(nAnnee, nMois, nJour, dFracDJour)))
    nRet |= VALEUR_AUTORISEE;

  // Cet algorithme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 7 : « Julian Day »
  // Les pages de 60 à 61

  int nAn = nAnnee;
  int nMs = nMois;
  double dJr = (double)nJour + dFracDJour;
  if (nMs == 1 || nMs == 2) {
    nAn -= 1;
    nMs += 12;
  }
  dJj = floor((double)365.25 * (double)(nAn + 4716)) +
        floor((double)30.6001 * (double)(nMs + 1)) + dJr -
        (double)1524.5;
  if (dJj < (double)0)
    nRet |= ERREUR_CALCUL;

  // Renvoyer la valeur du retour
  return nRet;
}

int ConvertirJourJulienEnGregorienne(double dJj, int& nAnnee,
                           int& nMois, int& nJour, double& dFracDJour) {
  int nRet = NORMAL;

  // Attraper les erreurs dans les arguments
  if (dJj < (double)0)
    nRet |= VALEUR_AUTORISEE;

  // Cet algorithme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 7 : « Julian Day »
  // La page 63

  double dT = dJj + (double)0.5;
  int nZ = (int)floor(dT);
  dFracDJour = dT - (double)nZ;
  int nAlpha =
       (int)floor(((double)nZ - (double)1867216.25) / (double)36524.25);
  int nA = nZ + 1 + nAlpha - (int)floor((double)nAlpha / (double)4);
  int nB = nA + 1524;
  int nC = (int)floor(((double)nB - (double)122.1) / (double)365.25);
  int nD = (int)floor((double)365.25 * (double)nC);
  int nE = (int)floor((double)(nB - nD) / (double)30.6001);
  nJour = nB - nD - (int)floor((double)30.6001 * (double)nE);
  if (nE < 14)
    nMois = nE - 1;
  else if (nE == 14 || nE == 15)
    nMois = nE - 13;
  else
    nRet |= ERREUR_CALCUL;
  if (nMois > 2)
    nAnnee = nC - 4716;
  else if (nMois == 1 || nMois == 2)
    nAnnee = nC - 4715;
  else
    nRet |= ERREUR_CALCUL;

  // Renvoyer la valeur du retour
  return nRet;
}

int ConvertirJourJulienEnJulienne(double dJj, int& nAnnee, int& nMois,
                                       int& nJour, double& dFracDJour) {
  int nRet = NORMAL;

  // Attraper les erreurs dans les arguments
  if (dJj < (double)0)
    nRet |= VALEUR_AUTORISEE;

  // Cet algorithme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 7 : « Julian Day »
  // La page 63

  double dT = dJj + (double)0.5;
  int nZ = (int)floor(dT);
  dFracDJour = dT - (double)nZ;
  int nA = nZ;
  int nB = nA + 1524;
  int nC = (int)floor(((double)nB - (double)122.1) / (double)365.25);
  int nD = (int)floor((double)365.25 * (double)nC);
  int nE = (int)floor((double)(nB - nD) / (double)30.6001);
  nJour = nB - nD - (int)floor((double)30.6001 * (double)nE);
  if (nE < 14)
    nMois = nE - 1;
  else if (nE == 14 || nE == 15)
    nMois = nE - 13;
  else
    nRet |= ERREUR_CALCUL;
  if (nMois > 2)
    nAnnee = nC - 4716;
  else if (nMois == 1 || nMois == 2)
    nAnnee = nC - 4715;
  else
    nRet |= ERREUR_CALCUL;

  // Renvoyer la valeur du retour
  return nRet;
}

int JourDeLaSemaine(double dJj) {

  // Cet algorithme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 7 : « Julian Day »
  // La page 65
  //
  // Cet algorithme a été modifié afin que le lundi est égale à zéro

  // Obtenir le jour julien à 0h
  double dJj0 = floor(dJj - (double)0.5);
  dJj0 += (double)0.5;

  // Calculer le jour de la semaine
  int nJourDSemaine = (int)(dJj0 + (double)0.5) % 7;
  if (nJourDSemaine < 0) {
    nJourDSemaine += 7; // si la valeur est négative, ajouter sept
    nJourDSemaine %= 7; // et réappliquer le modulo de sept
  }

  // Renvoyer le résultat
  return nJourDSemaine;
}

int JourDeLaSemaine_Gregorienne(int nAnnee, int nMois, int nJour,
                                                   int& nJourDSemaine) {
  // Calculer le jour julien à la date
  double dJj;
  int nRet = ConvertirGregorienneEnJourJulien(nAnnee, nMois, nJour,
                                                     (double)0.00, dJj);

  // Calculer le jour de la semaine du jour julien et renvoyer la
  // valeur du retour
  nJourDSemaine = JourDeLaSemaine(dJj);
  return nRet;
}

int JourDeLaSemaine_Julienne(int nAnnee, int nMois, int nJour,
                                                   int& nJourDSemaine) {
  // Calculer le jour julien à la date
  double dJj;
  int nRet = ConvertirJulienneEnJourJulien(nAnnee, nMois, nJour,
                                                      (double)0.0, dJj);

  // Calculer le jour de la semaine du jour julien et renvoyer la
  // valeur du retour
  nJourDSemaine = JourDeLaSemaine(dJj);
  return nRet;
}

int ConvertirJulienneEnGregorienne(int nAnnee_Juli, int nMois_Juli,
                      int nJour_Juli, int& nAnnee_Greg, int& nMois_Greg,
                                                      int& nJour_Greg) {
  int nRet = NORMAL;

  // Est-ce que la date julienne invalide ?
  if (!(
    DateValide_Julienne(nAnnee_Juli, nMois_Juli,
    nJour_Juli, (double)0.00)
  ))
    nRet |= VALEUR_AUTORISEE;

  // Calculer le jour julien de la date julienne ...
  double dJj, dFrac;
  nRet |= ConvertirJulienneEnJourJulien(nAnnee_Juli, nMois_Juli,
                                         nJour_Juli, (double)0.10, dJj);
  // La fraction du jour est pour la protection contre les erreurs
  // d'arrondi

  // ... et calculer la date grégorienne du jour julien
  nRet |= ConvertirJourJulienEnGregorienne(dJj, nAnnee_Greg,
                                         nMois_Greg, nJour_Greg, dFrac);
  // Renvoyer la valeur du retour
  return nRet;
}

bool DateDePaquesValide(int nMois, int nJour) {

  // Les dates valides pour Pâques sont entre le 22 mars et le 25 avril
  bool blValide = true;
  if (nMois == 3) { // Pâques dans le mars
    if (nJour < 22) // Pâques ne peux pas être avant le 22 mars
      blValide = false;
    if (nJour > 31) // Le jour de mars ne peux pas être après
      blValide = false; // le 31 mars
  } else if (nMois == 4) { // Pâques dans l'avril
    if (nJour > 25) // Pâques ne peux pas être après le 25 avril
      blValide = false;
    if (nJour < 1) // Le jour du mois ne peux pas être avant le
      blValide = false; // premier du mois
  } else { // Le mois n'est pas valide pour une date de Pâques
    blValide = false;
  }

  // Renvoyer le résultat de la validation
  return blValide;
}

int Paques_Greg(int nAnnee, int& nMois, int& nJour) {
  int nRet = NORMAL;

  // Est-ce que l'année avant le début du calendrier grégorien ?
  if (nAnnee < 1583)
    nRet |= VALEUR_APPROPRIEE;
  // Le calendrier grégorien commença le 15 octobre 1582 ; la première
  // Pâques après le début était en l'an 1583.

  // Cet algorithme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 8 : « Date of Easter »
  // Les pages de 67 à 68

  int nA = nAnnee % 19;
  int nB = (int)floor((double)nAnnee / (double)100);
  int nC = nAnnee % 100;
  int nD = (int)floor((double)nB / (double)4);
  int nE = nB % 4;
  int nF = (int)floor((double)(nB + 8) / (double)25);
  int nG = (int)floor((double)(nB - nF + 1) / (double)3);
  int nH = ((19 * nA) + nB - nD - nG + 15) % 30;
  int nI = (int)floor((double)nC / (double)4);
  int nK = nC % 4;
  int nL = (32 + (2 * nE) + (2 * nI) - nH - nK) % 7;
  int nM = (int)floor(
    (double)(nA + (11 * nH) + (22 * nL)) / (double)451
  );
  int nN = (int)floor(
    (double)(nH + nL - (7 * nM) + 114) / (double)31
  );
  int nP = (nH + nL - (7 * nM) + 114) % 31;
  nMois = nN;
  nJour = nP + 1;

  // Est-ce que la date de Pâques une date valide ?
  if (!(DateValide_Gregorienne(nAnnee, nMois, nJour, (double)0.00)))
    nRet |= ERREUR_CALCUL;
  if (!(DateDePaquesValide(nMois, nJour)))
    nRet |= ERREUR_CALCUL;

  // Renvoyer la valeur du retour
  return nRet;
}

int Paques_Juli(int nAnnee, int& nMois, int& nJour) {
  int nRet = NORMAL;

  // Est-ce que l'année avant le premier concile de Nicée ?
  if (nAnnee < 326)
    nRet |= VALEUR_APPROPRIEE;
  // Le concile était du 20 mai au 25 juillet 325 ; la première Pâques
  // après le consile était en l'an 326. Avant le concile il n'y avait
  // pas de norme pour le calcul de la date de Pâques.

  // Cet algorithme est adapté par :
  // « Astronomical Algorithms » par Jean Meeus
  // « First English Edition »
  // Chapître 8 : « Date of Easter »
  // La page 69

  int nA = nAnnee % 4;
  int nB = nAnnee % 7;
  int nC = nAnnee % 19;
  int nD = ((19 * nC) + 15) % 30;
  int nE = ((2 * nA) + (4 * nB) - nD + 34) % 7;
  int nF = (int)floor((double)(nD + nE + 114) / (double)31);
  int nG = (nD + nE + 114) % 31;
  nMois = nF;
  nJour = nG + 1;

  // Est-ce que la date de Pâques une date valide ?
  if (!(DateValide_Julienne(nAnnee, nMois, nJour, (double)0.00)))
    nRet |= ERREUR_CALCUL;
  if (!(DateDePaquesValide(nMois, nJour)))
    nRet |= ERREUR_CALCUL;

  // Renvoyer la valeur du retour
  return nRet;
}














int TrouverFetes() {
  int nRet = 0;
  int nMois, nJour, nJourDeSemaine;

  // Allouer de la mémoire pour le nom de la fête
  char *szFete = (char*)malloc(sizeof(char) * 128);

  // Trouver la date pour chaque fête spécifée
  if (g_blLundiPur) {
    nRet |= TrouvLundi(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[LUNDI_PUR]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blCendres) {
    nRet |= TrouvCendres(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[MERCREDI_CENDRES]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blRameaux) {
    nRet |= TrouvRameaux(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[DIMANCHE_RAMEAUX]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blVendredi) {
    nRet |= TrouvVendredi(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[VENDREDI_SAINT]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blPaques) {
    nRet |= TrouvPaques(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[PAQUES]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blAscension) {
    nRet |= TrouvAscension(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[JEUDI_ASCENSION]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blPentecote) {
    nRet |= TrouvPentecote(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[DIMANCHE_PENTECOTE]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blAvent) {
    nRet |= TrouvAvent(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[AVENT]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }
  if (g_blNoel) {
    nRet |= TrouvNoel(nMois, nJour, nJourDeSemaine);
    strcpy(szFete, g_szLesFetes[NOEL]);
    AfficherDate(g_nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }

  // Libérer la mémoire allouée
  free(szFete);
  return nRet;
}

int TrouverAnnees() {
  int nRet = 0;

  // Créer le tableau des années et allouer de la mémoire pour le nom
  // de la fête
  vector<int> vanAnnees;
  char *szFete = (char*)malloc(sizeof(char) * 128);

  // Trouver toutes les années dans la gamme d'années spécifiées et
  // copier le nom de la fête à « szFete »
  if (g_blLundiPur) {
    nRet |= Annees_Lundi(vanAnnees);
    strcpy(szFete, g_szLesFetes[LUNDI_PUR]);
  }
  else if (g_blCendres) {
    nRet |= Annees_Cendres(vanAnnees);
    strcpy(szFete, g_szLesFetes[MERCREDI_CENDRES]);
  }
  else if (g_blRameaux) {
    nRet |= Annees_Rameaux(vanAnnees);
    strcpy(szFete, g_szLesFetes[DIMANCHE_RAMEAUX]);
  }
  else if (g_blVendredi) {
    nRet |= Annees_Vendredi(vanAnnees);
    strcpy(szFete, g_szLesFetes[VENDREDI_SAINT]);
  }
  else if (g_blPaques) {
    nRet |= Annees_Paques(vanAnnees);
    strcpy(szFete, g_szLesFetes[PAQUES]);
  }
  else if (g_blAscension) {
    nRet |= Annees_Ascension(vanAnnees);
    strcpy(szFete, g_szLesFetes[JEUDI_ASCENSION]);
  }
  else if (g_blPentecote) {
    nRet |= Annees_Pentecote(vanAnnees);
    strcpy(szFete, g_szLesFetes[DIMANCHE_PENTECOTE]);
  }
  else if (g_blAvent) {
    nRet |= Annees_Avent(vanAnnees);
    strcpy(szFete, g_szLesFetes[AVENT]);
  }
  else if (g_blNoel) {
    nRet |= Annees_Noel(vanAnnees);
    strcpy(szFete, g_szLesFetes[NOEL]);
  }

  // Aucune fête n'est spécifiée ...
  else {
    MessageDErreur();
    vanAnnees.clear();
    free(szFete);
    return -1;
  }

  // Afficher les années trouvées
  AfficherAnnees(vanAnnees, szFete);

  // Libérer la mémoire pour les années et pour le nom de la fête
  vanAnnees.clear();
  free(szFete);

  // Renvoyer la valeur du retour
  return nRet;
}

int ListerDates() {
  int nRet = 0;
  int nMois, nJour, nJourDeSemaine;
  int (*fonctionDeTrouver)(int&,int&,int&);

  // Allouer de la mémoire pour le nom de la fête
  char *szFete = (char*)malloc(sizeof(char) * 128);

  // Définir le nom de la fête et affecter la fonction requise au
  // pointeur de fonction
  if (g_blLundiPur) {
    strcpy(szFete, g_szLesFetes[LUNDI_PUR]);
    fonctionDeTrouver = &TrouvLundi;
  }
  else if (g_blCendres) {
    strcpy(szFete, g_szLesFetes[MERCREDI_CENDRES]);
    fonctionDeTrouver = &TrouvCendres;
  }
  else if (g_blRameaux) {
    strcpy(szFete, g_szLesFetes[DIMANCHE_RAMEAUX]);
    fonctionDeTrouver = &TrouvRameaux;
  }
  else if (g_blVendredi) {
    strcpy(szFete, g_szLesFetes[VENDREDI_SAINT]);
    fonctionDeTrouver = &TrouvVendredi;
  }
  else if (g_blPaques) {
    strcpy(szFete, g_szLesFetes[PAQUES]);
    fonctionDeTrouver = &TrouvPaques;
  }
  else if (g_blAscension) {
    strcpy(szFete, g_szLesFetes[JEUDI_ASCENSION]);
    fonctionDeTrouver = &TrouvAscension;
  }
  else if (g_blPentecote) {
    strcpy(szFete, g_szLesFetes[DIMANCHE_PENTECOTE]);
    fonctionDeTrouver = &TrouvPentecote;
  }
  else if (g_blAvent) {
    strcpy(szFete, g_szLesFetes[AVENT]);
    fonctionDeTrouver = &TrouvAvent;
  }
  else if (g_blNoel) {
    strcpy(szFete, g_szLesFetes[NOEL]);
    fonctionDeTrouver = &TrouvNoel;
  }
  // ... il y a une erreur
  else {
    MessageDErreur();
    free(szFete);
    return -1;
  }

  // Parcourir chaque année, afficher la date de la fête pour chaque
  // année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= fonctionDeTrouver(nMois, nJour, nJourDeSemaine);
    AfficherDate(nAnnee, nMois, nJour, nJourDeSemaine, szFete);
  }

  // Libérer la mémoire allouée pour le nom de la fête et renvoyer
  // la valeur du retour
  free(szFete);
  return nRet;
}

int TrouvLundi(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Trouver la date de Pâques pour cette année
  nRet = TrouvPaques(nMois, nJour, nJourDeSemaine);

  // Convertir la date de Pâques de julienne/grégorienne à jour julien
  double dJourJulien, dX = (double)0.00;
  if (g_blAlexandrine && !(g_blGregorienne))
    ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else if (g_blGregorienne && !(g_blAlexandrine))
    ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg)
      ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
    else
      ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);

  // Soustraire quarante-huit jours pour le jour julien de lundi pur
  dJourJulien += (double)0.1;
  dJourJulien = (double)((int)floor(dJourJulien - (double)0.5) - 48) +
                (double)0.5;
  dJourJulien += (double)0.1;
  // l'haut est la protection contre les erreurs d'arrondi

  // Convertir le jour julien de lundi pur à julienne/grégorienne
  int nAn;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
    JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
    JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
      JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
    }
    else {
      ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
      JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
    }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvCendres(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Trouver la date de Pâques pour cette année
  nRet = TrouvPaques(nMois, nJour, nJourDeSemaine);

  // Convertir la date de Pâques de julienne/grégorienne à jour julien
  double dJourJulien, dX = (double)0.00;
  if (g_blAlexandrine && !(g_blGregorienne))
    ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else if (g_blGregorienne && !(g_blAlexandrine))
    ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg)
      ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
    else
      ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);

  // Soustraire quarante-six jours pour le jour julien de mercredi des
  // cendres
  dJourJulien += (double)0.1;
  dJourJulien = (double)((int)floor(dJourJulien - (double)0.5) - 46) +
                (double)0.5;
  dJourJulien += (double)0.1;
  // l'haut est la protection contre les erreurs d'arrondi

  // Convertir le jour julien de mercredi des cendres à
  // julienne/grégorienne
  int nAn;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
    JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
    JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
      JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
    }
    else {
      ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
      JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
    }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvRameaux(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Trouver la date de Pâques pour cette année
  nRet = TrouvPaques(nMois, nJour, nJourDeSemaine);

  // Convertir la date de Pâques de julienne/grégorienne à jour julien
  double dJourJulien, dX = (double)0.00;
  if (g_blAlexandrine && !(g_blGregorienne))
    ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else if (g_blGregorienne && !(g_blAlexandrine))
    ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg)
      ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
    else
      ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);

  // Soustraire sept jours pour le jour julien de dimanche des rameaux
  dJourJulien += (double)0.1;
  dJourJulien = (double)((int)floor(dJourJulien - (double)0.5) - 7) +
                (double)0.5;
  dJourJulien += (double)0.1;
  // l'haut est la protection contre les erreurs d'arrondi

  // Convertir le jour julien de dimanche des rameaux à
  // julienne/grégorienne
  int nAn;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
    JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
    JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
      JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
    }
    else {
      ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
      JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
    }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvVendredi(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Trouver la date de Pâques pour cette année
  nRet = TrouvPaques(nMois, nJour, nJourDeSemaine);

  // Convertir la date de Pâques de julienne/grégorienne à jour julien
  double dJourJulien, dX = (double)0.00;
  if (g_blAlexandrine && !(g_blGregorienne))
    ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else if (g_blGregorienne && !(g_blAlexandrine))
    ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg)
      ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
    else
      ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);

  // Soustraire deux jours pour le jour julien de vendredi saint
  dJourJulien += (double)0.1;
  dJourJulien = (double)((int)floor(dJourJulien - (double)0.5) - 2) +
                (double)0.5;
  dJourJulien += (double)0.1;
  // l'haut est la protection contre les erreurs d'arrondi

  // Convertir le jour julien de vendredi saint à julienne/grégorienne
  int nAn;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
    JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
    JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
      JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
    }
    else {
      ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
      JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
    }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvPaques(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Le calcul pour la méthode alexandrine seulement
  if (g_blAlexandrine && !(g_blGregorienne)) {
    nRet = Paques_Juli(g_nAnnee, nMois, nJour);
    JourDeLaSemaine_Julienne(g_nAnnee, nMois, nJour, nJourDeSemaine);
    if (g_blJuliAGreg) {
      int nAnnee_J = g_nAnnee;
      int nMois_J = nMois;
      int nJour_J = nJour;
      int nX;
      nRet |= ConvertirJulienneEnGregorienne(nAnnee_J, nMois_J, nJour_J,
                                                      nX, nMois, nJour);
    }
  }

  // Le calcul pour la méthode grégorienne seulement
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    nRet = Paques_Greg(g_nAnnee, nMois, nJour);
    JourDeLaSemaine_Gregorienne(g_nAnnee, nMois, nJour, nJourDeSemaine);
  }

  // Le calcul qui utilisant la méthode alexandrine avant l'année
  // spécifiée pour le changement et la méthode grégorienne après
  else {
    // Avant l'année du changement
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      nRet = Paques_Juli(g_nAnnee, nMois, nJour);
      JourDeLaSemaine_Julienne(g_nAnnee, nMois, nJour, nJourDeSemaine);
      if (g_blJuliAGreg) {
        int nAnnee_J = g_nAnnee;
        int nMois_J = nMois;
        int nJour_J = nJour;
        int nX;
        nRet |= ConvertirJulienneEnGregorienne(nAnnee_J, nMois_J,
                                             nJour_J, nX, nMois, nJour);
      }
    }
    // Après l'année du changement
    else {
      nRet = Paques_Greg(g_nAnnee, nMois, nJour);
      JourDeLaSemaine_Gregorienne(g_nAnnee, nMois, nJour,
                                                        nJourDeSemaine);
    }
  }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvAscension(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Trouver la date de Pâques pour cette année
  nRet = TrouvPaques(nMois, nJour, nJourDeSemaine);

  // Convertir la date de Pâques de julienne/grégorienne à jour julien
  double dJourJulien, dX = (double)0.00;
  if (g_blAlexandrine && !(g_blGregorienne))
    ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else if (g_blGregorienne && !(g_blAlexandrine))
    ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg)
      ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
    else
      ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);

  // Ajouter trente-neuf jours pour le jour julien de jeudi d'ascension
  dJourJulien += (double)0.1;
  dJourJulien = (double)((int)floor(dJourJulien - (double)0.5) + 39) +
                (double)0.5;
  dJourJulien += (double)0.1;
  // l'haut est la protection contre les erreurs d'arrondi

  // Convertir le jour julien de jeudi d'ascension à
  // julienne/grégorienne
  int nAn;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
    JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
    JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
      JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
    }
    else {
      ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
      JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
    }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvPentecote(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet;

  // Trouver la date de Pâques pour cette année
  nRet = TrouvPaques(nMois, nJour, nJourDeSemaine);

  // Convertir la date de Pâques de julienne/grégorienne à jour julien
  double dJourJulien, dX = (double)0.00;
  if (g_blAlexandrine && !(g_blGregorienne))
    ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else if (g_blGregorienne && !(g_blAlexandrine))
    ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg)
      ConvertirJulienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);
    else
      ConvertirGregorienneEnJourJulien(g_nAnnee, nMois, nJour, dX,
                                                           dJourJulien);

  // Ajouter quarante-neuf jours pour le jour julien de dimanche de
  // pentecôte
  dJourJulien += (double)0.1;
  dJourJulien = (double)((int)floor(dJourJulien - (double)0.5) + 49) +
                (double)0.5;
  dJourJulien += (double)0.1;
  // l'haut est la protection contre les erreurs d'arrondi

  // Convertir le jour julien de dimanche de pentecôte à
  // julienne/grégorienne
  int nAn;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
    JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
    JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
  }
  else
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJourJulien, nAn, nMois, nJour, dX);
      JourDeLaSemaine_Julienne(nAn, nMois, nJour, nJourDeSemaine);
    }
    else {
      ConvertirJourJulienEnGregorienne(dJourJulien, nAn, nMois, nJour,
                                                                    dX);
      JourDeLaSemaine_Gregorienne(nAn, nMois, nJour, nJourDeSemaine);
    }

  // Renvoyer le nombre du retour
  return nRet;
}

int TrouvAvent(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet = 0;

  // Calculer le jour julien de Noël pour cette année et le jour de la
  // semaine
  double dJour;
  double dX = (double)0.10;
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJulienneEnJourJulien(g_nAnnee, 12, 25, dX, dJour);
  }
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirGregorienneEnJourJulien(g_nAnnee, 12, 25, dX, dJour);
  }
  else {
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJulienneEnJourJulien(g_nAnnee, 12, 25, dX, dJour);
    }
    else {
      ConvertirGregorienneEnJourJulien(g_nAnnee, 12, 25, dX, dJour);
    }
  }
  nJourDeSemaine = JourDeLaSemaine(dJour);

  // Combien jours à la quatrième dimanche d'avent ?
  int nJoursAvant = nJourDeSemaine + 1;

  // Obtenir le jour julien du quatrième dimanche d'avent
  // depuis le jour julien du première dimanche d'avent
  dJour -= (double)nJoursAvant; // à le 4ème dimanche
  dJour -= (double)21;          // à le 1re dimanche
  nJourDeSemaine = JourDeLaSemaine(dJour);

  // Calculer la date de la première dimanche d'avent ...
  int nX;

  // ... Utilisant julienne seulement
  if (g_blAlexandrine && !(g_blGregorienne)) {
    ConvertirJourJulienEnJulienne(dJour, nX, nMois, nJour, dX);
  }

  // ... Utilisant grégorienne seulement
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    ConvertirJourJulienEnGregorienne(dJour, nX, nMois, nJour, dX);
  }

  // ... Utilisant la julienne pour les années avant l'année du
  // changement ou la grégorienne pour les années après
  else {
    // Avant l'année du changement
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      ConvertirJourJulienEnJulienne(dJour, nX, nMois, nJour, dX);
    }
    // Après l'année du changement
    else {
      ConvertirJourJulienEnGregorienne(dJour, nX, nMois, nJour, dX);
    }
  }


  return nRet;
}

int TrouvNoel(int& nMois, int& nJour, int& nJourDeSemaine) {
  int nRet = 0;

  // Noël est le 25 décembre toujours
  nMois = 12;
  nJour = 25;

  // Pour le calendrier julien seulement
  if (g_blAlexandrine && !(g_blGregorienne)) {
    JourDeLaSemaine_Julienne(g_nAnnee, nMois, nJour, nJourDeSemaine);
    if (g_blJuliAGreg) {
      int nX;
      int nAnnee_J = g_nAnnee;
      int nMois_J = nMois;
      int nJour_J = nJour;
      nRet |= ConvertirJulienneEnGregorienne(nAnnee_J, nMois_J, nJour_J,
                                                      nX, nMois, nJour);
    }
  }

  // Pour le calendrier grégorien seulement
  else if (g_blGregorienne && !(g_blAlexandrine)) {
    JourDeLaSemaine_Gregorienne(g_nAnnee, nMois, nJour, nJourDeSemaine);
  }

  // Pour le calendrier julien avant l'année spécifiée pour le
  // changement et le calendrier grégorien après
  else {
    // Avant l'année du changement
    if (g_nAnnee < g_nAnnee_JuliChangeGreg) {
      JourDeLaSemaine_Julienne(g_nAnnee, nMois, nJour, nJourDeSemaine);
      if (g_blJuliAGreg) {
        int nX;
        int nAnnee_J = g_nAnnee;
        int nMois_J = nMois;
        int nJour_J = nJour;
        nRet |= ConvertirJulienneEnGregorienne(nAnnee_J, nMois_J,
                                             nJour_J, nX, nMois, nJour);
      }
    }
    // Après l'année du changement
    else {
      JourDeLaSemaine_Gregorienne(g_nAnnee, nMois, nJour,
                                                        nJourDeSemaine);
    }
  }

  // Renvoyer le nombre du retour
  return nRet;
}

int Annees_Lundi(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvLundi(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Cendres(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvCendres(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Rameaux(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvRameaux(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Vendredi(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvVendredi(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Paques(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvPaques(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Ascension(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvAscension(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Pentecote(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvPentecote(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Avent(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvAvent(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

int Annees_Noel(vector<int>& vanAnnees) {
  int nRet = 0;
  int nMois_Test, nJour_Test, nJDS;

  // Parcourir chaque année
  for (int nAnnee = g_nPremiereAnnee; nAnnee <= g_nDerniereAnnee;
                                                             nAnnee++) {
    g_nAnnee = nAnnee;
    nRet |= TrouvNoel(nMois_Test, nJour_Test, nJDS);
    // Si les date corresponent, ajouter l'année à la liste
    if (nMois_Test == g_nMoisFete && nJour_Test == g_nJourFete)
      vanAnnees.push_back(nAnnee);
  }

  // Renvoyer la valeur du retour
  return nRet;
}

